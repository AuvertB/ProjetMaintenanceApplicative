#ifndef _ID_H_
#  define _ID_H_

#  define ID_FILE "id.txt"

unsigned long long int get_next_id();

void set_id(unsigned long long int id);

void save_id();

void load_id();


#endif
